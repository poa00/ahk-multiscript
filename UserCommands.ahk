; ﻿; Created by Asger Juul Brunshøj

; Note: Save with encoding UTF-8 with BOM if possible.
; I had issues with special characters like in ¯\_(ツ)_/¯ that wouldn't work otherwise.
; Notepad will save UTF-8 files with BOM automatically (even though it does not say so).
; Some editors however save without BOM, and then special characters look messed up in the AHK GUI.




;-------------------------------------------------------------------------------
;;; SEARCH OTHER THINGS ;;;
;-------------------------------------------------------------------------------
if Pedersen = s%A_Space% ; Search Google
    Search("Search", "https://google.com/search?q=REPLACEME")
else if Pedersen = y%A_Space% ; Search Youtube
    Search("Youtube", "https://www.youtube.com/results?search_query=REPLACEME")
else if Pedersen = t%A_Space%
    Search("Thesaurus", "https://www.thesaurus.com/browse/REPLACEME?s=t")
else if Pedersen = en%A_Space%
    Search("En -> Fr", "https://www.wordreference.com/enfr/REPLACEME")
else if Pedersen = fr%A_Space%
    Search("Fr -> En", "https://www.wordreference.com/fren/REPLACEME")
else if Pedersen = /%A_Space%
    Search("/r/", "https://www.teddit.net/r/REPLACEME")
else if Pedersen = w%A_Space%
    Search("Wikipedia", "https://en.wikipedia.org/w/index.php?search=REPLACEME")
else if Pedersen = g%A_Space%
    Search("Github", "https://github.com/search?q=REPLACEME")
else if Pedersen = gs%A_Space%
    Search("Google Scholar", "https://scholar.google.com/scholar?q=REPLACEME")
else if Pedersen = cpp
    Search("CppReference", "https://fr.cppreference.com/mwiki/index.php?search=REPLACEME")
else if Pedersen = def
    Search("Disctionary", "https://dictionary.cambridge.org/dictionary/english/REPLACEME")
else if Pedersen = tldr
    Search("Tldr", "https://tldr.ostera.io/REPLACEME")

;-------------------------------------------------------------------------------
;;; LAUNCH WEBSITES AND PROGRAMS ;;;
;-------------------------------------------------------------------------------
else if Pedersen = red
    RunCmd("www.reddit.com")
else if Pedersen = maps
    RunCmd("https://maps.google.com")
else if Pedersen = hn
    RunCmd("https://news.ycombinator.com")
else if Pedersen = flight
    RunCmd("https://www.flightradar24.com/")
else if Pedersen = ff
    RunCmd("firefox.exe")
else if Pedersen = out
    RunCmd("OUTLOOK.EXE")
else if Pedersen = moz
    RunCmd("C:\Program Files (x86)\Mozilla Thunderbird\thunderbird.exe")
else if Pedersen = fip
    RunCmd("https://fip.fr")
else if Pedersen = met
    RunCmd("https://userportal.metsafecloud.com/login")
else if Pedersen = calc
    RunCmd("Calc.exe")
else if Pedersen = yed
   RunCmd("C:\Program Files\yWorks\yEd\yEd.exe")
else if Pedersen = python
   RunCmd("python3.exe")
else if Pedersen = term
     RunCmd("c:/Users/benoit.viry/opt/cmder/cmder.exe")
else if Pedersen = date
{
    gui_destroy() ; removes the GUI even when the reload fails
    FormatTime, CurrentDateTime,, yyyy-MM-dd
    SendInput %CurrentDateTime%
}


;-------------------------------------------------------------------------------
;;; INTERACT WITH THIS AHK SCRIPT ;;;
;-------------------------------------------------------------------------------
else if Pedersen = rel ; Reload this script
{
    gui_destroy() ; removes the GUI even when the reload fails
    Reload
}
else if Pedersen = host ; Edit host script
{
    gui_destroy()
    run, notepad++.exe "" "%A_ScriptFullPath%"
}
else if Pedersen = sync
{
    gui_destroy()
    run, cmd.exe /C scp C:\Users\benoit.viry\.config\Database.kdbx pi@192.168.1.40:/home/pi/.config
}
else if Pedersen = user ; Edit GUI user commands
{
    gui_destroy()
    run, notepad++.exe "" "%A_ScriptDir%\UserCommands.ahk"
}

else if Pedersen = dir ; Open the directory for this script
{
    gui_destroy()
    Run, %A_ScriptDir%
}
else if Pedersen = down ; Downloads
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Downloads")
}
else if Pedersen = home
{
    gui_destroy()
    run C:\Users\%A_Username%
}
else if Pedersen = name
{
    gui_destroy()
    Send, Benoit VIRY
}
else if Pedersen = rec ; Recycle Bin
{
    gui_destroy()
    Run ::{645FF040-5081-101B-9F08-00AA002F954E}
}
else if Pedersen = dia
{
    ; RunCmd("D:\Dev\SafeNcy\docs\img\diagrams")
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\doc_svn\Safency\3 Technical Docs (spec, manuals, tests)\5-Diagrams")
}
else if Pedersen = svn
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\doc_svn")
}
else if Pedersen = templates
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\Doc CGX\Templates documents CGX")
}
else if Pedersen = notes
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\notes")
}
else if Pedersen = ds
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\doc_svn\Safency")
}
else if Pedersen = tmp
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\tmp")
}
else if Pedersen = cs
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\workspace\SafeNcy\SafeNcy")
}
else if Pedersen = cw
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents\workspace\Weather")
}
else if Pedersen = doc
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\Users\benoit.viry\Documents")
}
else if Pedersen = root
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe C:\")
}
else if Pedersen = rpi
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe \\RASPBERRYPI")
}
else if Pedersen = music
{
    RunCmd("C:\Users\benoit.viry\opt\explorer++\explorer++.exe \\RASPBERRYPI\Musiques")
}


;-------------------------------------------------------------------------------
;;; MISCELLANEOUS ;;;
;-------------------------------------------------------------------------------
else if Pedersen = @
{
    gui_destroy()
    Send, benoit.viry@cgx-group.com
}
else if Pedersen = tel
{
    gui_destroy()
    Send, 0563378291
}
else if Pedersen = wifi
{
    gui_destroy()
    Send, dYyM3xhU
}
else if Pedersen = rma
{
    gui_destroy()
    Send, {Tab}
    Sleep, 100
    Send, {Tab}
    Sleep, 100
    Send, a{Tab}
    Sleep, 200
    Send, sa{Tab}
    Sleep, 100
    Send, thales fr{Tab}
    Sleep, 100
    Send, {Down}{Tab}
    Sleep, 100
    Send, pro{Tab}
    Sleep, 100
    Send, SafeNcy -
}
else if Pedersen = ? ; Tooltip with list of commands
{
    GuiControl,, Pedersen, ; Clear the input box
    Gosub, gui_commandlibrary
}
else if Pedersen = acc
{
    ToolTip, Multiline`nTooltip, A_ScreenWidth //2, A_ScreenHeight //2
    return
}
else if Pedersen = test
{
    strOut := RunWaitOne("powershell ""[convert]::ToBase64String([Text.Encoding]::UTF8.GetBytes(\""itviry\""))""")
    ToolTip, %strOut%, A_ScreenWidth //2, A_ScreenHeight //2
    return
}
else if Pedersen = ac
{
    gui_destroy()
    Send, 36000{Tab}
    Sleep, 100
    Send, 220{Tab}
    Sleep, 200
    Send, 0.7
}

RunCmd(cmd)
{
    gui_destroy()
    exeWin := % "ahk_exe " . cmd
    if WinExist(exeWin)
    {
        WinActivate
    }
    else
    {
        RunWait %cmd%,,,OutputVarPID
        WinSet, AlwaysOnTop, On, ahk_pid %OutputVarPID%
    }
}

Search(title, url)
{
    gui_destroy()
    global gui_search_title
    gui_search_title = %title%
    gui_search(url)
}

Login(username, pwd)
{
    gui_destroy()
    Send, %username%{Tab}%pwd%
}

RunWaitOne(command) {
    ; WshShell object: http://msdn.microsoft.com/en-us/library/aew9yb99
    shell := ComObjCreate("WScript.Shell")
    ; Execute a single command via cmd.exe
    exec := shell.Exec(ComSpec " /C " command)
    ; Read and return the command's output
    return exec.StdOut.ReadAll()
}
