#SingleInstance force

Bind(cmd)
{
    if((getkeystate("shift", "p") and (getkeystate("control", "p")))) {
        SendInput, +^{%cmd%}
    } else if(getkeystate("shift", "p")) {
        SendInput, +{%cmd%}
    } else if(getkeystate("control", "p")) {
        SendInput, ^{%cmd%}
    } else {
        SendInput, {%cmd%}
    }
    return
}

*#f:: Bind("PgUp") return
*#s:: Bind("PgDn") return
*#r:: Bind("home") return
*#t:: Bind("end") return

*#n:: Bind("left") return
*#i:: Bind("right") return
*#e:: Bind("down") return
*#u:: Bind("up") return

; *#d:: Bind("^left") return
; *#h:: Bind("^right") return
*#h:: Send, ^{right}
*#d:: Send, ^{left}

#q:: SendInput ALzQbtXPMg8bRo0hRWG82FfxivTORZwP

LWin & WheelUp::Send {Volume_Up}
return
LWin & WheelDown::Send {Volume_Down}
return
LWin & MButton::Send {Volume_Mute}
return


#Escape::
    clip := CopyToClipboard()
    if (!clip) {
        return
    }
    addr := ExtractAddress(clip)
    if (!addr)
    {
        addr := "https://www.google.com/search?q=" . clip
    }

    Run %addr%
    return

;; utility functions

;; Safely copies-to-clipboard, restoring clipboard's original value after
;; Returns the captured clip text, or "" if unsuccessful after 4 seconds
CopyToClipboard()
{
    ; Wait for modifier keys to be released before we send ^C
    KeyWait LWin
    KeyWait Alt
    KeyWait Shift
    KeyWait Ctrl

    ; Capture to clipboard, then restore clipboard's value from before capture
    ExistingClipboard := ClipboardAll
    Clipboard =
    SendInput ^{Insert}
    ClipWait, 4
    NewClipboard := Clipboard
    Clipboard := ExistingClipboard
    if (ErrorLevel)
    {
        MsgBox, The attempt to copy text onto the clipboard failed.
        return ""
    }
    return NewClipboard
}

;; Extracts an address from anywhere in str.
;; Recognized addresses include URLs, email addresses, domain names, Windows local paths, and Windows UNC paths.
ExtractAddress(str)
{
    if (RegExMatch(str, "S)((http|https|ftp|mailto)://[\S]+)", match))
        return match1
    if (RegExMatch(str, "S)(\w+@[\w.]+\.(com|net|org|gov|cc|edu|info|fr))", match))
        return "mailto:" . match1
    if (RegExMatch(str, "S)(www\.\S+)", match))
        return "http://" . match1
    if (RegExMatch(str, "S)(\w+\.(com|net|org|gov|cc|edu|info|fr))", match))
        return "http://" . match1
    if (RegExMatch(str, "S)([a-zA-Z]:[\\/][\\/\-_.,\d\w\s]+)", match))
        return match1
    if (RegExMatch(str, "S)(\\\\[\w\-]+\\.+)", match))
        return match1
    return ""
}
